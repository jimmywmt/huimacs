# HUIMACS
This is the source codes for the paper:
> Wu, Jimmy Ming-Tai, Justin Zhan, and Jerry Chun-Wei Lin. "An ACO-based approach to mine high-utility itemsets." *Knowledge-Based Systems* 116 (2017): 102-113.

-------
## Compile
1. mkdir build
2. cd build
3. cmake -DCMAKE_BUILD_TYPE=Release ../
4. make

-------
## Usage
usage: ./huim_acs --file=string --threshold=double [options] ...

options:

      -f, --file         database file path (string)
      
      -t, --threshold    high utility itemset threshold (double)
      
      -m, --max          maximum iteration (unsigned int [=10000])
      
      -s, --size         population size (unsigned int [=20])
      
      -p, --pheromone    inital pheromone (double [=1])
      
      -r, --rho          rho (double [=0.4])
      
      -a, --alpha        alpha (double [=1])
      
      -b, --beta         beta (double [=3])
      
      -q, --q0           q0 (double [=0.8])
      
      -i, --interval     result interval (unsigned int [=100])
      
      -w, --write        write file
      
      -?, --help         print this message



