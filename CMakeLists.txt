cmake_minimum_required(VERSION 3.7)
project(HUIMACS)
set(CMAKE_CXX_STANDARD 14)

include_directories(${HUIMACS_SOURCE_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${HUIMACS_BINARY_DIR})

add_subdirectory(src)

